Not all random values are created equal - for security-related code, you need a *specific kind* of random value.

A summary of this article, if you don't want to read the entire thing:

* __Don't use `Math.random()`.__ There are *extremely* few cases where `Math.random()` is the right answer. Don't use it, unless you've read this *entire* article, and determined that it's necessary for your case.
* __Don't use `crypto.getRandomBytes` directly.__ While it's a CSPRNG, it's easy to bias the result when 'transforming' it, such that the output becomes more predictable.
* __If you want to generate random tokens or API keys:__ Use [`uuid`](https://www.npmjs.com/package/uuid), specifically the `uuid.v4()` method. Avoid `node-uuid` - it's not the same package, and doesn't produce reliably secure random values.
* __If you want to generate random numbers in a range:__ Use [`random-number-csprng`](https://www.npmjs.com/package/random-number-csprng).

You should seriously consider reading the entire article, though - it's not *that* long :)

## Types of "random"

There exist roughly three types of "random":

* __Truly random:__ Exactly as the name describes. True randomness, to which no pattern or algorithm applies. It's debatable whether this really exists.
* __Unpredictable:__ Not *truly* random, but impossible for an attacker to predict. This is what you need for security-related code - it doesn't matter *how* the data is generated, as long as it can't be guessed.
* __Irregular:__ This is what most people think of when they think of "random". An example is a game with a background of a star field, where each star is drawn in a "random" position on the screen. This isn't truly random, and it isn't even unpredictable - it just doesn't *look* like there's a pattern to it, visually.

*Irregular* data is fast to generate, but utterly worthless for security purposes - even if it doesn't seem like there's a pattern, there is almost always a way for an attacker to predict what the values are going to be. The only realistic usecase for irregular data is things that are represented visually, such as game elements or randomly generated phrases on a joke site.

*Unpredictable* data is a bit slower to generate, but still fast enough for most cases, and it's sufficiently hard to guess that it will be attacker-resistant. Unpredictable data is provided by what's called a __CSPRNG__.

## Types of RNGs (Random Number Generators)

* __CSPRNG:__ A *Cryptographically Secure Pseudo-Random Number Generator*. This is what produces *unpredictable* data that you need for security purposes.
* __PRNG:__ A *Pseudo-Random Number Generator*. This is a broader category that includes CSPRNGs *and* generators that just return irregular values - in other words, you *cannot* rely on a PRNG to provide you with unpredictable values.
* __RNG:__ A *Random Number Generator*. The meaning of this term depends on the context. Most people use it as an even *broader* category that includes PRNGs and *truly* random number generators.

Every random value that you need for security-related purposes (ie. anything where there exists the possibility of an "attacker"), should be generated using a __CSPRNG__. This includes verification tokens, reset tokens, lottery numbers, API keys, generated passwords, encryption keys, and so on, and so on.

## Bias

In Node.js, the most widely available CSPRNG is the `crypto.randomBytes` function, but *you shouldn't use this directly*, as it's easy to mess up and "bias" your random values - that is, making it more likely that a specific value or set of values is picked.

A common example of this mistake is using the `%` modulo operator when you have less than 256 possibilities (since a single byte has 256 possible values). Doing so actually makes lower values *more likely* to be picked than higher values.

For example, let's say that you have 36 possible random values - `0-9` plus every lowercase letter in `a-z`. A naive implementation might look something like this:

```js
let randomCharacter = randomByte % 36;
```

__That code is broken and insecure.__ With the code above, you essentially create the following ranges (all inclusive):

* __0-35__ stays 0-35.
* __36-71__ becomes 0-35.
* __72-107__ becomes 0-35.
* __108-143__ becomes 0-35.
* __144-179__ becomes 0-35.
* __180-215__ becomes 0-35.
* __216-251__ becomes 0-35.
* __252-255__ becomes *0-3*.

If you look at the above list of ranges you'll notice that while there are __7 possible values__ for each `randomCharacter` between 4 and 35 (inclusive), there are __8 possible values__ for each `randomCharacter` between 0 and 3 (inclusive). This means that while there's a __2.64% chance__ of getting a value between 4 and 35 (inclusive), there's a __3.02% chance__ of getting a value between 0 and 3 (inclusive).

This kind of difference may *look* small, but it's an easy and effective way for an attacker to reduce the amount of guesses they need when bruteforcing something. And this is only *one* way in which you can make your random values insecure, despite them originally coming from a secure random source.

## So, how do I obtain random values securely?

In Node.js:

* __If you need a sequence of random bytes:__ Use [`crypto.randomBytes`](https://nodejs.org/dist/latest-v18.x/docs/api/crypto.html#cryptorandombytessize-callback).
* __If you need individual random numbers in a certain range:__ use [`crypto.randomInt`](https://nodejs.org/dist/latest-v18.x/docs/api/crypto.html#cryptorandomintmin-max-callback).
* __If you need a random string:__ You have two good options here, depending on your needs.
  1. Use a v4 UUID. Safe ways to generate this are [`crypto.randomUUID`](https://nodejs.org/dist/latest-v18.x/docs/api/crypto.html#cryptorandomuuidoptions), and [the `uuid` library](https://www.npmjs.com/package/uuid) (only the v4 variant!).
  2. Use a nanoid, using the [`nanoid` library](https://www.npmjs.com/package/nanoid). This also allows specifying a custom alphabet to use for your random string.

Both of these use a CSPRNG, and 'transform' the bytes in an unbiased (ie. secure) way.

In the browser:

* When using the Node.js options, your bundler *should* automatically select equivalently safe browser implementations for all of these.
* If not using a bundler:
  * __If you need a sequence of random bytes:__ Use [`crypto.getRandomValues`](https://developer.mozilla.org/en-US/docs/Web/API/Crypto/getRandomValues) with a `Uint8Array`. Other array types will get you numbers in different ranges.
  * __If you need a random string:__ You have two good options here, depending on your needs.
    1. Use a v4 UUID, with the [`crypto.randomUUID`](https://developer.mozilla.org/en-US/docs/Web/API/Crypto/randomUUID) method.
    2. Use a nanoid, using the **standalone build** of the [`nanoid` library](https://github.com/ai/nanoid#install). This also allows specifying a custom alphabet to use for your random string.
    
However, it is __strongly__ recommended that you use a bundler, in general.
